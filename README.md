# Food database

### Prerequisites

OS tested:

- Ubuntu 16.04

## Usage

    ```
    python graph.py
    ```

## Solution

This program runs test provided in the assignment README

## Authors

Yassine Boudi
