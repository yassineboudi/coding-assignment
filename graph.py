#!usr/bin/python
# -*- coding: utf-8 -*-

from database import Database
import json
import unittest


def create_graph(build, extract, edits):

    status = {}
    if len(build) > 0:
        # Build graph
        db = Database(build[0][0])

        if len(build) > 1:
            db.add_nodes(build[1:])

        # Add extract
        db.add_extract(extract)

        # Graph edits
        db.add_nodes(edits)

        # Update status
        status = db.get_extract_status()

    return status


def testGraphStatus():
    build = [("core", None), ("A", "core"),
             ("B", "core"), ("C", "core"), ("C1", "C")]
    # Extract
    extract = {"img001": ["A", "B"], "img002": [
        "A", "C1"], "img003": ["B", "E"]}
    # Graph edits
    edits = [("A1", "A"), ("A2", "A"), ("C2", "C")]

    result_status = create_graph(build, extract, edits)
    expected_status = {"img001": "granularity_staged",
                       "img002": "coverage_staged", "img003": "invalid"}

    unittest.TestCase().assertDictEqual(result_status, expected_status)

    with open('res/graph_build.json') as json_file:
        build_data = json.load(json_file)

    with open('res/graph_edits.json') as json_file:
        edits_data = json.load(json_file)

    with open('res/img_extract.json') as json_file:
        extract_data = json.load(json_file)

    with open('res/expected_status.json') as json_file:
        expected_status_data = json.load(json_file)

    result_status_data = create_graph(build_data, extract_data, edits_data)

    unittest.TestCase().assertDictEqual(result_status_data, expected_status_data)


if __name__ == '__main__':

    try:
        testGraphStatus()
    except:
        print("Error !")
        raise
    print("Tests passed")
