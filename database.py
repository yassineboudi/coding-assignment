#!usr/bin/python
# -*- coding: utf-8 -*-


class Database(object):

    def __init__(self, id):

        self.id = id
        self.graph_dict = {id: []}
        self.extract_dict = {}
        self.graph_status = {}

    def add_nodes(self, nodes):
        """ Adds node to graph.

        Parameters:
            nodes (list): list of tuples (id_new_node, id_parent_node)

        Returns:
            add nodes to the graph and updates status.
        """
        for element in nodes:
            node_parent = element[1]
            node_child = element[0]
            self.graph_dict.setdefault(node_parent, []).append(node_child)

            for key in self.extract_dict:
                label_list = self.extract_dict[key]
                # verify for each element in label_list if its a leaf or not
                leafs_list = list(map(self.__is_leaf, label_list))
                if self.graph_status[key] != "coverage_staged":
                    self.__update_granularity(key, leafs_list)

            self.__update_coverage(node_parent)

    def add_extract(self, extract_dict):
        """ stores information appropriately.
        Parameters:
            extract_dict (dict): dict where keys are image names, and values are list of class/node IDs

        Returns:
            stores dict.
        """
        for key in extract_dict:
            label_list = extract_dict[key]
            valid_list = list(map(self.__verify_label, label_list))

            if False in valid_list:
                self.graph_status[key] = "invalid"
            else:
                self.extract_dict[key] = label_list
                leafs_list = list(map(self.__is_leaf, label_list))
                self.__update_granularity(key, leafs_list)

    def __update_granularity(self, key, leafs_list):
        """ updates graph status for granularity cases.
        Parameters:
            key (string): keys image name as defined in graph status
            leafs_list (list): list of nodes that has a children node or not
        Returns:
            update granularity.
        """
        self.graph_status[key] = "granularity_staged" if False in leafs_list else "valid"

    def __update_coverage(self, node_parent):
        """ updates graph status for coverage cases.
        Parameters:
            node_parent (string): node parent
        Returns:
            update coverage.

        """
        if self.extract_dict:
            for key in self.extract_dict:
                if self.graph_status[key] != "invalid":
                    label_list = self.extract_dict[key]
                    # get labels (parent or child) assoiated with key (image)
                    for label in label_list:
                        if self.__get_parent(label) == node_parent:
                            self.graph_status[key] = "coverage_staged"

    def get_extract_status(self):
        """ return graph status .
        Returns:
            (dict): dict of the graph status.
        """

        return self.graph_status

    def __is_leaf(self, node):
        """ verify if the node has a child node or not : leaf.
        Parameters:
            node (string): node parent
        Returns:
            Boolean.
        """
        return not node in self.graph_dict

    def __verify_label(self, label):
        """ verify if the node label exists in the graph.
        Parameters:
            label (string): label name

        Returns:
            Boolean.
        """
        exists = False
        for key in self.graph_dict:
            label_list = self.graph_dict[key]
            if label in label_list or label in self.graph_dict:
                exists = True
                break
        return exists

    def __get_parent(self, node):
        """ gets the parent of node.
        Parameters:
            node (string): node name
        Returns:
            key (string): key name in the graph created
        """
        for key in self.graph_dict:
            parent_list = self.graph_dict[key]
            if node in parent_list:
                return key
